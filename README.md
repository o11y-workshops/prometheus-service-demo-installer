Prometheus Services Demo Installer
==================================
This demo makes use of the Prometheus Demo Service
(https://github.com/juliusv/prometheus_demo_service) and is built from source
for use in our workshop.

Install on your local machine
-----------------------------
This is an installation from the source code of this services demo project, you
will build it from scratch.

**Prerequisites:** Go version 1.18+

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/prometheus-service-demo-installer/-/archive/main/prometheus-service-demo-installer-main.zip)

2. Run 'init.sh' file. 

3. Follow the instructions in the terminal output to start the services.

Install in a container
----------------------

**Prerequisites:** Podman v4.5.1+

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/prometheus-service-demo-installer/-/archive/main/prometheus-service-demo-installer-main.zip)

2. Build the container: 'podman build -t prometheus_services_demo -f installs/demo/Buildfile'.

3. Run the container:  'podman run -d -p 8080:8080 prometheus_services_demo'.

4. To add another serivces demo, just run the container and map the ports to available ranges, such as: 'podman run -d -p 8081:8080 prometheus_services_demo'.

Validating installation
-----------------------
You can test the metrics are being generated after starting the services at:

```
http://localhost:8080/metrics  (or 8081 port if you started a second instance)
```


Getting started
---------------
Not sure how to get started with this services demo? See it in action in this
<a href="https://o11y-workshops.gitlab.io/workshop-prometheus" target="_blank">online workshop:</a>

----------------------------------------------

<a href="https://o11y-workshops.gitlab.io/workshop-prometheus" target="_blank"><img src="docs/demo-images/workshop.png" width="70%"></a>

----------------------------------------------

Notes:
-----
If for any reason the installation breaks or you want to start anew, just rerun the installation script to
reinstall the services demo project.


Supporting Articles
-------------------
- [O11y Guide - Your First Steps in Cloud Native Observability](https://www.schabell.org/2022/09/o11y-guide-your-first-steps-in-cloud-native-observability.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.0 - All inclusive project to install demo services for Prometheus workshop.

