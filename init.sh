#!/bin/bash

DEMO="Prometheus Service Demo Installer"
AUTHORS="Eric D. Schabell"
PROJECT="git@gitlab.com:o11y-workshops/prometheus-service-demo-installer.git"

# variables used globally in sourced functions.
export MAJ_GO_VERSION=1
export MIN_GO_VERSION=18
export INSTALL_DIR=./installs
export DEMO_DIR=${INSTALL_DIR}/demo

# wipe screen.
clear

echo
echo "#################################################3#########################"
echo "##                                                                       ##"
echo "##  Setting up the ${DEMO}                     ##"
echo "##                                                                       ##"
echo "##      ####  ####   ###  #   # ##### ##### #   # ##### #   #  ####      ##"
echo "##      #   # #   # #   # ## ## #       #   #   # #     #   # #          ##"
echo "##      ####  ####  #   # # # # ###     #   ##### ###   #   #  ###       ##"
echo "##      #     #  #  #   # #   # #       #   #   # #     #   #     #      ##"
echo "##      #     #   #  ###  #   # #####   #   #   # #####  ###  ####       ##"
echo "##                                                                       ##"
echo "##               #### ##### ##### #   # #####  #### #####                ##"
echo "##              #     #     #   # #   #   #   #     #                    ##"
echo "##               ###  ###   ##### #   #   #   #     ###                  ##"
echo "##                  # #     #  #   # #    #   #     #                    ##"
echo "##              ####  ##### #   #   #   #####  #### #####                ##"
echo "##                                                                       ##"
echo "##                     ####  ##### #   #  ###                            ##"
echo "##                     #   # #     ## ## #   #                           ##"
echo "##                     #   # ###   # # # #   #                           ##"
echo "##                     #   # #     #   # #   #                           ##"
echo "##                     ####  ##### #   #  ###                            ##"
echo "##                                                                       ##"
echo "##        ##### #   #  #### #####  ###  #     #     ##### #####          ##"
echo "##          #   ##  # #       #   #   # #     #     #     #   #          ##"
echo "##          #   # # #  ###    #   ##### #     #     ###   #####          ##"
echo "##          #   #  ##     #   #   #   # #     #     #     #  #           ##"
echo "##        ##### #   # ####    #   #   # ##### ##### ##### #   #          ##"
echo "##                                                                       ##"
echo "##  brought to you by ${AUTHORS}                                   ##"
echo "##                                                                       ##"
echo "##  ${PROJECT}  ##"
echo "##                                                                       ##"
echo "###########################################################################"
echo

# Check for GO installation.
command -v go version -v >/dev/null 2>&1 || { echo >&2 "Go language is required but not installed yet... download and install here: https://go.dev/doc/install"; exit; }

# Version check for Go.
echo "Checking for Go version needed:"
maj_version=$(go version | cut -d" " -f3 | cut -d "." -f1 | cut -d "o" -f2)
min_version=$(go version | cut -d" " -f3 | cut -d "." -f2)

if [ "${maj_version}" -ge "${MAJ_GO_VERSION}" ]; then
  echo "Go major version is good..."
  if [ "${min_version}" -ge "${MIN_GO_VERSION}" ]; then
    echo "Go minor version is good..."
    echo "Your Go version ${maj_version}.${min_version} accepted!"
    echo
  fi
else
  echo "Your Go version must be ${GO_VERSION} or higher..."
  exit;
fi

echo "Building the services.."
echo
command cd ${DEMO_DIR}
command rm -f services_demo
command go mod download

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during 'go mod download' command..."
	echo
	exit;
fi

command go build -v -o services_demo .

if [ $? -ne 0 ]; then
  echo
	echo "Error occurred during 'go build' command..."
	echo
	exit;
fi

echo "Prometheus service project built successfully!"
echo
echo "======================================================"
echo "=                                                    ="
echo "=  Installer complete, get ready to rock!            ="
echo "=                                                    ="
echo "=  The Prometheus services can be started from a     ="
echo "=  terminal with the following command:              ="
echo "=                                                    ="
echo "=     $ ${DEMO_DIR}/services_demo                ="
echo "=                                                    ="
echo "=  You can make sure the service is running by       ="
echo "=  viewing the exported metrics at:                  ="
echo "=                                                    ="
echo "=        http://localhost:8080/metrics               ="
echo "=                                                    ="
echo "======================================================"
echo
